-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tiendavirtual10
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tiendavirtual10
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendavirtual10` DEFAULT CHARACTER SET utf8 ;
USE `tiendavirtual10` ;

-- -----------------------------------------------------
-- Table `mydb`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual10`.`producto` (
  `idProducto` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `valorCompra` DECIMAL(10,2) NOT NULL,
  `valorVenta` DECIMAL(10,2) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`idProducto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual10`.`transaccion` (
  `idTransaccion` INT NOT NULL,
  `tipoTransaccion` VARCHAR(45) NOT NULL,
  `fechaTransaccion` DATE NOT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`idTransaccion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendavirtual10`.`detalle` (
  `idDetalle` INT NOT NULL,
  `producto_idProducto` INT NOT NULL,
  `transaccion_idTransaccion` INT NOT NULL,
  `valorDetalle` DECIMAL(10,2) NOT NULL,
  `cantidadDetalle` INT NOT NULL,
  `totalDetalle` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`idDetalle`, `producto_idProducto`, `transaccion_idTransaccion`),
  INDEX `fk_producto_has_transaccion_transaccion1_idx` (`transaccion_idTransaccion` ASC) VISIBLE,
  INDEX `fk_producto_has_transaccion_producto_idx` (`producto_idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_producto_has_transaccion_producto`
    FOREIGN KEY (`producto_idProducto`)
    REFERENCES `mydb`.`producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_has_transaccion_transaccion1`
    FOREIGN KEY (`transaccion_idTransaccion`)
    REFERENCES `mydb`.`transaccion` (`idTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
