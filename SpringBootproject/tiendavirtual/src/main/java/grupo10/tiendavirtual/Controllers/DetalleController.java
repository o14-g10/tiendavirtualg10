/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupo10.tiendavirtual.Controllers;

import grupo10.tiendavirtual.Modelo.Detalle;
import grupo10.tiendavirtual.Services.DetalleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lucar
 */
@RestController
@CrossOrigin
@RequestMapping("/detalle")
public class DetalleController {

    @Autowired
    private DetalleService detalleservice;
    
    
    //Consultar todo
    
    @GetMapping("/list")
    public List<Detalle> consultarTodo() {
        return detalleservice.findAll();
    }
    
    
    //Consultar por ID
    
    @GetMapping("/list/{id}")
    public Detalle consultaPorId(@PathVariable Integer id) {
        return detalleservice.findById(id);
    }
    
    
    //Agregar
    
    @PostMapping(value = "/")
    public ResponseEntity<Detalle> agregar(@RequestBody Detalle detalle) {
        Detalle obj = detalleservice.save(detalle);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
    //Eliminar
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Detalle> eliminar(@PathVariable Integer id) {
        Detalle obj = detalleservice.findById(id);
        if (obj != null) {
            detalleservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
    //Actualizar
    
    @PutMapping(value = "/")
    public ResponseEntity<Detalle> editar(@RequestBody Detalle detalle) {
        Detalle obj = detalleservice.findById(detalle.getIdDetalle());
        if (obj != null) {
            obj.setProducto(detalle.getProducto());
            obj.setTransaccion(detalle.getTransaccion());
            obj.setCantidadDetalle(detalle.getCantidadDetalle());
            obj.setTotalDetalle(detalle.getTotalDetalle());
            obj.setValorDetalle(detalle.getValorDetalle());
            detalleservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

}
