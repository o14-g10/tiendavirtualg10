/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupo10.tiendavirtual.Controllers;

import grupo10.tiendavirtual.Modelo.Transaccion;
import grupo10.tiendavirtual.Services.TransaccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lucar
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/transaccion")
public class TransaccionController {

    @Autowired
    private TransaccionService transaccionservice;
    
    //Consultar todo
    
    @GetMapping("/list")
    public List<Transaccion> consultarTodo() {
        return transaccionservice.findAll();
    }
    
    //Consultar por ID
    
    @GetMapping("/list/{id}")
    public Transaccion consultaPorId(@PathVariable Integer id) {
        return transaccionservice.findById(id);
    }
    
    
    //Agregar
    
    @PostMapping(value = "/")
    public ResponseEntity<Transaccion> agregar(@RequestBody Transaccion transaccion) {
        Transaccion obj = transaccionservice.save(transaccion);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
    //Eliminar
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Transaccion> eliminar(@PathVariable Integer id) {
        Transaccion obj = transaccionservice.findById(id);
        if (obj != null) {
            transaccionservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
    //Actualizar
    
    @PutMapping(value = "/")
    public ResponseEntity<Transaccion> editar(@RequestBody Transaccion transaccion) {
        Transaccion obj = transaccionservice.findById(transaccion.getIdTransaccion());
        if (obj != null) {
            obj.setFechaTransaccion(transaccion.getFechaTransaccion());
            obj.setTipoTransaccion(transaccion.getTipoTransaccion());
            obj.setTipoTransaccion(transaccion.getTipoTransaccion());
            obj.setTotal(transaccion.getTotal());
            transaccionservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
}
