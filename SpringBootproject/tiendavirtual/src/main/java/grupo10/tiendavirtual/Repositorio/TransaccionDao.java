/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package grupo10.tiendavirtual.Repositorio;

import grupo10.tiendavirtual.Modelo.Transaccion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lucar
 */

@Repository
public interface TransaccionDao extends CrudRepository<Transaccion, Integer>{
    
}
