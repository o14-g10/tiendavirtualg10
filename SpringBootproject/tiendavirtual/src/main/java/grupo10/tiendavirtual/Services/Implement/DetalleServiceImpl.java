/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupo10.tiendavirtual.Services.Implement;

import grupo10.tiendavirtual.Modelo.Detalle;
import grupo10.tiendavirtual.Modelo.Producto;
import grupo10.tiendavirtual.Repositorio.DetalleDao;
import grupo10.tiendavirtual.Services.DetalleService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucar
 */

@Service
public class DetalleServiceImpl implements DetalleService{
    
    @Autowired
    private DetalleDao detalleDao;
    
    @Override
    @Transactional
    public Detalle save(Detalle detalle){
    return detalleDao.save(detalle);
    }
    @Override
    @Transactional
    public void delete(Integer id){
    detalleDao.deleteById(id);
    }
    @Override
    @Transactional
    public Detalle findById(Integer id){
    return detalleDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional
    public List<Detalle> findAll(){
    return (List<Detalle>) detalleDao.findAll();
    }
}
