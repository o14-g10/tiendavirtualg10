/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupo10.tiendavirtual.Services.Implement;

import grupo10.tiendavirtual.Modelo.Producto;
import grupo10.tiendavirtual.Repositorio.ProductoDao;
import grupo10.tiendavirtual.Services.ProductoService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucar
 */

@Service
public class ProductoServiceImpl implements ProductoService{
    
    @Autowired
    private ProductoDao productoDao;
    
    @Override
    @Transactional
    public Producto save(Producto producto){
    return productoDao.save(producto);
    }
    
    @Override
    @Transactional
    public void delete(Integer id){
    productoDao.deleteById(id);
    }
    
    @Override
    @Transactional
    public Producto findById(Integer id){
    return productoDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional
    public List<Producto> findAll(){
    return (List<Producto>) productoDao.findAll();
    }
}
