/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupo10.tiendavirtual.Services.Implement;

import grupo10.tiendavirtual.Modelo.Transaccion;
import grupo10.tiendavirtual.Repositorio.TransaccionDao;
import grupo10.tiendavirtual.Services.TransaccionService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lucar
 */

@Service
public class TransaccionServiceImpl implements TransaccionService{
    @Autowired
    private TransaccionDao transaccionDao;
    
    @Override
    @Transactional
    public Transaccion save(Transaccion transaccion){
    return transaccionDao.save(transaccion);
    }
    @Override
    @Transactional
    public void delete(Integer id){
    transaccionDao.deleteById(id);
    }
    @Override
    @Transactional
    public Transaccion findById(Integer id){
    return transaccionDao.findById(id).orElse(null);
    }
    @Override
    @Transactional
    public List<Transaccion> findAll(){
    return (List<Transaccion>) transaccionDao.findAll();
    }
}
