/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package grupo10.tiendavirtual.Services;

import grupo10.tiendavirtual.Modelo.Transaccion;
import java.util.List;

/**
 *
 * @author lucar
 */

public interface TransaccionService {
    public Transaccion save(Transaccion transaccion);
    public void delete(Integer id);
    public Transaccion findById(Integer id);
    public List<Transaccion> findAll();
}
